package me.Printendo.HewwoStudio;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;

public class HewwoController implements Initializable {
    
    public class Check {
        private final String name;
        private final String start;
        private final String finish;
       
        
        public Check(String name, String start, String finish) {
            this.name = name;
            this.start = start;
            this.finish = finish;
        }
        public String getName(){
            return name; 
        }
        public String getRegex(){
            return start; 
        }
        public String getFinish(){
            return finish; 
        }
    }
    
    List<Check> checks = new ArrayList<>();
    
    @FXML 
    private TextArea input, result;
    
    @FXML 
    private List<CheckBox> checkboxes;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        checks.add(new Check("sowta", "(\\bsort of\\b)|(\\bsortof\\b)", "sowta"));
        checks.add(new Check("okay", "\\bokay\\b", "otay"));
        checks.add(new Check("my", "\\bmy\\b", "muh"));
        checks.add(new Check("wont", "(\\bwill not\\b)|(\\bwon't\\b)|(\\bwont\\b)", "wun"));
        checks.add(new Check("you", "\\byou", "yew"));
        checks.add(new Check("is", "\\bis\\b", "ish"));
        checks.add(new Check("dont", "(\\bdo not\\b)|(\\bdon't\\b)|(\\bdont\\b)", "dun"));
        checks.add(new Check("toilet", "(\\btoilet)|(\\brestroom)|(\\bloo\\b)", "potty"));
        checks.add(new Check("not", "(\\bnot\\b)", "nawt"));
        checks.add(new Check("stop", "(\\bstop)", "stahp"));
        checks.add(new Check("thats", "(\\bthat is\\b)|(\\bthat's\\b)|(\\bthats\\b)", "das"));
        checks.add(new Check("swear", "(\\bfucker\\b)|(\\bwanker\\b)|(\\bugger\\b)", "hecker"));
        checks.add(new Check("swear", "\\bshit", "poop"));
        checks.add(new Check("swear", "\\bpiss", "pee"));
        checks.add(new Check("swear", "\\bdamn", "dang"));
        checks.add(new Check("swear", "\\bfuck", "heck"));
        checks.add(new Check("swear", "(\\bdick)|(\\bchoad)|(\\bchode)|(\\btwat)|(\\bcunt)", "peepee"));
        checks.add(new Check("rl", "[rl]", "w"));
        checks.add(new Check("fullstop", "[.!]", " x3"));
        checks.add(new Check("thd", "\\bth", "d"));
        checks.add(new Check("thf", "th", "f"));
        checks.add(new Check("qu", "qu", "kw"));
        checks.add(new Check("apos", "'", ""));
    }
    
    @FXML
    private void convert(ActionEvent event) {
        String x = input.getText().toLowerCase();
        
        List<String> checkedBoxes = new ArrayList<>();
        
        checkboxes.stream().filter((checkbox) -> (checkbox.isSelected())).forEachOrdered((checkbox) -> {
            checkedBoxes.add(checkbox.getId());
        });
        
        for (Check replace : checks)
        {
            if (checkedBoxes.contains(replace.getName()))
            {
                x = x.replaceAll(replace.getRegex(), replace.getFinish());
            }
        }
        
        result.setText(x);
        
    }    
}
